package com.ruben.simplestreamquery.controller;

import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.ruben.simplestreamquery.pojo.po.UserInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * TestController
 *
 * @author VampireAchao
 * @since 2022/10/5
 */
@RestController
@RequestMapping("test")
public class TestController {

    @GetMapping
    public List<UserInfo> list() {
        return Db.list(UserInfo.class);
    }

}
