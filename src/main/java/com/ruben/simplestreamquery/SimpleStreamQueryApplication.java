package com.ruben.simplestreamquery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleStreamQueryApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleStreamQueryApplication.class, args);
    }

}
