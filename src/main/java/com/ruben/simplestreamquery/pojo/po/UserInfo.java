package com.ruben.simplestreamquery.pojo.po;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.Version;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.time.LocalDateTime;

/**
 * UserInfo
 *
 * @author VampireAchao
 * @since 2022/5/21
 */
@Data
@Builder
public class UserInfo {

    private String email;

    private static final long serialVersionUID = -7219188882388819210L;
    private Long id;
    private String name;
    private Integer age;
    private String mobile;

    @Tolerate
    public UserInfo() {
        // this is an accessible parameterless constructor.
    }

    @Version
    private Integer version;
    @TableLogic(value = "'2001-01-01 00:00:00'", delval = "NOW()")
    private LocalDateTime gmtDeleted;
}
