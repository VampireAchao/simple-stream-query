package com.ruben.simplestreamquery.pojo.po;

import lombok.Data;

/**
 * RoleInfo
 *
 * @author VampireAchao
 * @since 2022/5/23
 */
@Data
public class RoleInfo {

    private String id;
    private String roleName;
}
