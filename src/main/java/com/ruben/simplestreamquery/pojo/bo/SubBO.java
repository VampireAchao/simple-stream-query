package com.ruben.simplestreamquery.pojo.bo;

import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author VampireAchao
 * @since 2023/3/16 10:18
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SubBO<T, K extends Comparable<? super K> & Serializable, S> extends BaseDbBO<S> {

    private List<T> mainList;
    private List<SFunction<T, K>> subIdGetters;
    private List<SFunction<T, S>> subGetters;
    private List<SFunction<S, ?>> subCompares;

    public SubBO() {
        super();
        this.mainList = new ArrayList<>();
        this.subIdGetters = new ArrayList<>();
        this.subGetters = new ArrayList<>();
        this.subCompares = new ArrayList<>();
    }
}
