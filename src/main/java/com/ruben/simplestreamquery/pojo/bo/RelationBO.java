package com.ruben.simplestreamquery.pojo.bo;

import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author VampireAchao
 * @since 2023/3/6 11:32
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RelationBO<T,
        K extends Comparable<? super K> & Serializable,
        A,
        U extends Comparable<? super U> & Serializable,
        R> extends BaseDbBO<R> {

    private List<T> mainList;
    private SFunction<T, K> mainKey;
    private SFunction<A, U> attachKey;
    private SFunction<T, List<A>> attachGetter;
    private SFunction<R, K> relationMain;
    private SFunction<R, U> relationAttach;
    private List<SFunction<A, Object>> attachCompares;
    private List<SFunction<R, Object>> relationCompares;
    private Boolean insertOnMissAttach;

    public RelationBO() {
        super();
        attachCompares = new ArrayList<>();
        relationCompares = new ArrayList<>();
        insertOnMissAttach = false;
    }
}
