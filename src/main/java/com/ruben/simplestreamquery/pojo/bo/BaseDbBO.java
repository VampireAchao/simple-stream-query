package com.ruben.simplestreamquery.pojo.bo;

import io.github.vampireachao.stream.core.lambda.function.SerCons;
import io.github.vampireachao.stream.plugin.mybatisplus.Database;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author VampireAchao
 * @since 2023/3/16 11:07
 */
@Data
public abstract class BaseDbBO<T> {

    private List<T> willInsertList;
    private List<T> willDeleteList;
    private List<T> willUpdateList;

    private List<T> dataList;
    private Consumer<BaseDbBO<T>> afterExecuted;

    public BaseDbBO() {
        dataList = new ArrayList<>();
        willInsertList = new ArrayList<>();
        willDeleteList = new ArrayList<>();
        willUpdateList = new ArrayList<>();
        afterExecuted = SerCons.nothing();
    }

    public List<T> execute() {
        Database.removeByIds(getWillDeleteList());
        Database.saveFewSql(getWillInsertList());
        Database.updateFewSql(getWillUpdateList());
        afterExecuted.accept(this);
        return dataList;
    }

    public BaseDbBO<T> add(BaseDbBO<T> bo) {
        this.getWillInsertList().addAll(bo.getWillInsertList());
        this.getWillDeleteList().addAll(bo.getWillDeleteList());
        this.getWillUpdateList().addAll(bo.getWillUpdateList());
        this.afterExecuted = SerCons.multi(this.afterExecuted::accept, bo.getAfterExecuted()::accept);
        return this;
    }
}
