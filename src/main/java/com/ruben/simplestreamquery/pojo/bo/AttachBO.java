package com.ruben.simplestreamquery.pojo.bo;

import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author VampireAchao
 * @since 2023/3/15 16:38
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AttachBO<T,
        K extends Comparable<? super K> & Serializable,
        A> extends BaseDbBO<A> {

    private List<T> mainList;
    private SFunction<T, K> mainKey;
    private SFunction<A, K> attachKey;
    private SFunction<T, List<A>> attachGetter;
    private List<SFunction<A, Object>> attachCompares;


    public AttachBO() {
        super();
        attachCompares = new ArrayList<>();
    }
}
