package com.ruben.simplestreamquery.pojo.vo;

import cn.hutool.core.lang.Dict;
import org.springframework.http.HttpStatus;

/**
 * GlobalResult
 *
 * @author VampireAchao
 * @since 2022/10/5
 */
public class GlobalResult extends Dict {

    public GlobalResult() {
        super(4);
        set("code", HttpStatus.OK.value())
                .set("success", true)
                .set("msg", HttpStatus.OK.series().name())
                .set("data", null);
    }

    public static GlobalResult ok() {
        return new GlobalResult();
    }

    public static GlobalResult error() {
        GlobalResult globalResult = new GlobalResult();
        globalResult.set("code", HttpStatus.INTERNAL_SERVER_ERROR.value())
                .set("success", false)
                .set("msg", HttpStatus.INTERNAL_SERVER_ERROR.series().name());
        return globalResult;
    }

    public static GlobalResult ok(Object data) {
        GlobalResult globalResult = new GlobalResult();
        globalResult.set("data", data);
        return globalResult;
    }


}
