package com.ruben.simplestreamquery;

import cn.hutool.core.util.ClassUtil;
import com.ruben.simplestreamquery.pojo.po.UserInfo;
import io.github.vampireachao.stream.plugin.mybatisplus.Database;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SimpleStreamQueryApplicationTests {

    @Test
    void testQuery() {
        List<UserInfo> list = Database.list(UserInfo.class);
        Assertions.assertFalse(list.isEmpty());
        ClassUtil.scanPackage();
        Database.updateFewSql(list);
    }

}
